#-------------------------------------------------------------------------------
# Name:        google_jam.py
# Purpose:	   For Fun; Google Code Jam 2014 World Finals Problem A
#
# Description: Determine the number of row/col swaps to convert a 2D matrix into 
#			   a checkerboard.  
# https://code.google.com/codejam/contest/7214486/dashboard
#-------------------------------------------------------------------------------

#where nxn is the size of 2D matrix m
def count_defects (n, m):
	order=[]
	num_eq=num_neq=0

	#Compare each row to the first row
	for row in range (0,n):
		eq=neq=True
		for col in range (0,n):
			if (m[0][col] == m[row][col]):
				neq=False
			else:
				eq=False
		#Row must be either completely equal or completely not equal
		if ((not eq) and (not neq)):
			return "IMPOSSIBLE"
		if (eq):
			num_eq+=1
			order.append(True)
		else:
			num_neq+=1
			order.append(False)
	#Number of equal and unequal rows must be the same
	if not (num_neq==num_eq):
		return "IMPOSSIBLE"

	defects=0
	#Assumes the order of equal and unequal rows begins with equal
	for i in range (0,n,2):
		if (order[i]==False):
			defects+=1

	#(n/2-defects) is the number of defects assuming the order begins with not equal
	return min(defects, n/2-defects)

def solve_matrix(n,m):
	ans=count_defects(n,m)
	if (ans=="IMPOSSIBLE"):
		return ans
	#Tranpose matrix to count defects for columns
	ans2=count_defects(n,zip(*m))
	if not (ans2=="IMPOSSIBLE"):
		#Total number of swaps is the sum because row operations and col operations are independent
		return ans+ans2
	else:
		return ans2

def read_matrix(file_name):
	f=open(file_name, 'r')
	g=open("google_output.txt",'w')
	num_tests=int(f.readline())

	for i in range (0,num_tests):
		m=[]
		size=2 * int(f.readline())
		for j in range (0,size):
			m.append(list(f.readline())[:-1])
		
		ans=solve_matrix(size,m)
		g.write("Case #"+ str(i+1)+ ": "+ str(ans)+"\n")

	f.close()
	g.close()



def main():
	#read_matrix("A-large-practice.txt")
	read_matrix("A-small-practice.txt")

if __name__ == '__main__':
    main()
